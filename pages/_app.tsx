import { Provider as EffectorProvider } from 'effector-react/ssr';
import { fork, Scope, serialize } from 'effector';
import React from 'react';
import { AppProps } from 'next/app';

let clientScope: Scope;

export default function MyApp({ Component, pageProps }: AppProps) {
  const scope = fork({
    values: {
      ...(clientScope ? serialize(clientScope) : {}),
      ...pageProps.initialState,
    },
  });

  if (typeof window !== undefined) {
    clientScope = scope;
  }

  return (
    <EffectorProvider value={scope}>
      <Component {...pageProps} />
    </EffectorProvider>
  );
}

import type { GetServerSidePropsContext, NextPage } from 'next';
import { getInitialState } from '@lib/get-initial-state';
import { AboutPage, pageLoaded } from '@pages/about';

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { props } = await getInitialState(pageLoaded, context);

  return { props };
}

const About: NextPage = () => {
  return <AboutPage />;
};

export default About;

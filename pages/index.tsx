import type { GetServerSidePropsContext, NextPage } from 'next';
import { getInitialState } from '@lib/get-initial-state';
import { HomePage, pageLoaded } from '@pages/home';

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const { props } = await getInitialState(pageLoaded, context);

  return { props };
}

const Home: NextPage = () => {
  return <HomePage />;
};

export default Home;

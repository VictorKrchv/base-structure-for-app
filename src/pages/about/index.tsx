import { Container } from '@ui';

import * as model from './model';

export { pageLoaded } from './model';

export const AboutPage = () => {
  return <Container>AboutPage</Container>;
};

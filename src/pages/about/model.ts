import { createEvent } from 'effector';
import { GetServerSidePropsContext } from 'next';

export const pageLoaded = createEvent<GetServerSidePropsContext>();

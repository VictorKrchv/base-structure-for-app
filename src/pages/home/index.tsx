import { Container } from '@ui';

import * as model from './model';

export const HomePage = () => {
  return <Container>HomePage</Container>;
};

export { pageLoaded } from './model';

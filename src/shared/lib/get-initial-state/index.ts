import { fork, allSettled, serialize, Unit } from 'effector';

export async function getInitialState<P>(unit: Unit<P>, params?: P) {
  const scope = fork();

  await allSettled(unit, { scope, params });

  const initialState = serialize(scope);

  return { scope, props: { initialState } };
}

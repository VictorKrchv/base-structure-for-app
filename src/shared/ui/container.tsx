import styled from 'styled-components';

export const Container = styled.div`
  max-width: 1200px;
  width: 100%;
  padding: 15px;
  margin: 0 auto;
`